This is a Node.js script for automating the claiming of rewards for multiple Blurt accounts using the @blurtfoundation/blurtjs library.
Requirements

    Node.js
    Blurt account(s) with private posting key(s)
    @blurtfoundation/blurtjs library

Installation

    Clone this repository or download the files to your computer.
    Install @blurtfoundation/blurtjs library by running npm install @blurtfoundation/blurtjs --save.
    Edit the script with your own account names and their corresponding private posting keys.
    Optionally, change the timer interval or the RPC node.
    Run the script with node claim-rewards.js.

Configuration

    myAccounts: Add the account names with their corresponding private posting keys, separated by commas. The last account should not have a comma at the end of its line.
    interval: Set the timer interval in minutes for how often the script will claim rewards.
    rpc: Set your RPC node URL, or leave https://rpc.blurt.world for fast performance.

Usage

    Navigate to the directory where the script is located in the terminal.
    Run the script with node blurt-rewards.js.
    The script will print the current date and time, then proceed to check if the RPC node is reachable.
    For each account in the myAccounts object, the script will get the rewards available to claim and proceed to claim them if possible.
    The script will print the account name, claimed amount of Blurt Power, and the reward_vests claimed as SP.

Notes

    To stop the script, you can call abortTimer() function or press CTRL+C.
    Claiming rewards will consume Resource Credits (RC), so be sure to have enough RC available for the transaction.
    The script will claim all available rewards in one transaction, so be sure to run it frequently enough to avoid missing out on any rewards.